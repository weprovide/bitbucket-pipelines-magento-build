#!/bin/sh

# colors
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

CACHETOOL_VERSION="7.1.0" # cachetool 7.1.0 supports PHP >= 7.3 (https://github.com/gordalina/cachetool#compatibility)
CACHETOOL_SHA256="6aee9b6c504be5a9f4159b78c86ceb8dca06bc29c0cda13f9485cca1eaf82118" # SHA256 for version 7.1.0 of cachetool

#set vars if not set
if [ -z $SSH_PORT ]; then SSH_PORT=22; fi
if [ -z $ENV_PATH ]; then ENV_PATH="/env.php"; fi
if [ -z $MEDIA_PATH ]; then MEDIA_PATH="/media"; fi
if [ -z $RELEASES_PATH ]; then RELEASES_PATH="/releases"; fi
if [ -z $NPM_PARAMS ]; then NPM_PARAMS=""; fi
if [ -z $ROOT ]; then ROOT="/data/web"; fi
if [ -z $SSH_USER ]; then SSH_USER="app"; fi
if [ -z $SSH_PHP_VERSION ]; then SSH_PHP_VERSION="/usr/bin/php73"; fi
if [ -z $NODE_VERSION ]; then NODE_VERSION="10"; fi

# Clean OPCache using the cachetool by gordalina (https://github.com/gordalina/cachetool)
echo "if [ ! -d '${ROOT}/.bin' ]; then
  mkdir -p '${ROOT}/.bin'
fi

if [ ! -f '${ROOT}/.bin/cachetool' ] || [ $CACHETOOL_SHA256 != \$(sha256sum '${ROOT}/.bin/cachetool' | cut -d ' ' -f 1) ]; then
  echo 'Downloading version ${CACHETOOL_VERSION} of cachetool (\"https://github.com/gordalina/cachetool\")'
  curl -s -o '${ROOT}/.bin/cachetool' 'https://gordalina.github.io/cachetool/downloads/cachetool-${CACHETOOL_VERSION}.phar'

  # check integrity of the download to ensure that it was not compromised, SHA256 hash should be kept up-to-date in the future when the cachetool is updated
  if [ $CACHETOOL_SHA256 != \$(sha256sum '${ROOT}/.bin/cachetool' | cut -d ' ' -f 1) ]; then
    echo 'Failed to verify the integrity of the cachetool, please make sure that it was not compromised'
    exit 1
  fi

  chmod +x '${ROOT}/.bin/cachetool'
fi" | ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT

apt-get update --allow-releaseinfo-change
apt-get install -y gnupg
apt-get install -y build-essential

# output php version
php -v

# composer
composer install --no-dev
if [ $? -ne 0 ]
then
    echo -e "\n\n"
    echo -e "${RED}Composer install failed or threw up, aborting...${NC}"
    exit 1
fi

# npm
file="aviate.config.js"
if [ -f "$file" ]
then
    apt-get install -y apt-transport-https ca-certificates

    #install node
    curl -sL https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash -
    apt-get install -y nodejs

    #install correct math expression evaluator version
    npm install math-expression-evaluator@1.2.22 --save
    npm install --package-lock

    # compile CSS and JS
	npm_command="npm run build --verbose"
    if [ ! -z "$NPM_PARAMS" ]
    then
        npm_command="${NPM_PARAMS} npm run build --verbose"
    fi
    echo $npm_command
    eval $npm_command
    if [ $? -ne 0 ]
    then
        echo -e "\n\n"
        echo -e "${RED}npm run build failed, you might loose proper styling, aborting...${NC}"
        exit 1
    fi
fi

# change permissions for running commands
chmod +x bin/magento
# check if magento is successfully installed before we use it
bin/magento list
if [ $? -ne 0 ]
then
    echo -e "\n\n"
    echo -e "${RED}bin/magento broken, aborting...${NC}"
    exit 1
fi
# disable all dev tools
bin/magento module:disable Magento_Version
bin/magento module:disable LexBeelen_Ngrok
bin/magento module:disable WeProvide_MSPAmastyFix
bin/magento module:disable MSP_DevTools
bin/magento module:disable MSP_Common
bin/magento module:disable Jh_BlockLogger

# get translation files from server if in gitignore before building content
if [[ $(grep 'app/i18n' ./.gitignore) ]]
then
    echo -e "\n"
    echo -e "${BLUE}translation files ignored, fetch from server...${NC}"
    rsync -r -e "ssh -p $SSH_PORT" ${SSH_USER}@${SSH_HOST}:${ROOT}${FILES_PATH}/latestrelease/app/i18n ./app
fi

# get translation files from server if they are maintainable using the admin (WeProvide_Files module)
modulestatus=$(bin/magento module:status WeProvide_Files)
if [[ $modulestatus = "Module is enabled" ]]
then
    echo -e "\n"
    echo -e "${BLUE}i18n files maintainable on server, fetch from server...${NC}"

    i18ndirs=$(ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "${SSH_PHP_VERSION} ${ROOT}${FILES_PATH}/latestrelease/bin/magento weprovide:i18n:info --path-only --relative")
    for dir in $i18ndirs
    do
        echo -e "${BLUE}  fetching ${dir}...${NC}"
        rsync -r -e "ssh -p $SSH_PORT" ${SSH_USER}@${SSH_HOST}:${ROOT}${FILES_PATH}/latestrelease${dir} .${dir}
    done
fi


export HTTPS=on

# deploy theme and language, deploys per language and therefore deploys less
themes=bitbucket-pipelines-themes.json
if [ -f "$themes" ]; then
    themes=$(<bitbucket-pipelines-themes.json)
    for row in $(echo "${themes}" | jq -r '.[] | @base64'); do
        _jq() {
          echo ${row} | base64 --decode | jq -r ${1}
        }
        languages=$(_jq '.themes' | jq 'map("--theme \(.)")' | jq -r 'join(" ")');
        php -dmemory_limit=4G bin/magento setup:static-content:deploy -f $(_jq '.language') $languages;
        if [ $? -ne 0 ]
        then
            echo -e "\n\n"
            echo -e "${RED}bin/magento setup:static-content:deploy threw up, aborting...${NC}"
            exit 1
        fi
    done
else
    # deploy theme and language in one go
    configfile=bitbucket-pipelines-config.json
    if [ -f "$configfile" ]; then
        # read languages from file
        languages=$(jq -r '.languages | map(.) | join(" ")' bitbucket-pipelines-config.json)
        # read themes from file
        themes=$(jq -r '.themes | map("--theme \(.)") | join(" ")' bitbucket-pipelines-config.json)

        php -dmemory_limit=4G bin/magento setup:static-content:deploy -f $languages $themes
        if [ $? -ne 0 ]
        then
            echo -e "\n\n"
            echo -e "${RED}bin/magento setup:static-content:deploy threw up, aborting...${NC}"
            exit 1
        fi
    else
        echo -e "\n\n"
        echo -e "${RED}bin/magento setup:static-content:deploy is missing required configuration, aborting...${NC}"
        exit 1
    fi
fi

# compile code with increased memory limit
php -dmemory_limit=4G bin/magento setup:di:compile
if [ $? -ne 0 ]
then
    echo -e "\n\n"
    echo -e "${RED}bin/magento setup:di:compile threw up, aborting...${NC}"
    exit 1
fi

# create new pathname
RELEASEPATH=$(date +%s)

# create a public folder for the website if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$PUBLIC_PATH"
# create a folder for the website files if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH"
# create a media folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH${MEDIA_PATH}"
# create a sitemaps folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH/sitemaps"
# create a log folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH/log"
# create a logging (admin actions) folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH/logging"
# create a report folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH/report"

# create a import folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH/import"
# create a importexport folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH/importexport"
# create a import_history folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH/import_history"

# create a private folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH/private"
# create a sitemaps folder in the release if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/pub/sitemaps"
# create a releases folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH${RELEASES_PATH}"
# create a folder with this pathname on the server
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH"

# create lock file to let scripts know build is busy
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "echo '' > ${ROOT}$FILES_PATH/build.lock"

# tar and move all compiled code to server
touch website.tar.gz
tar --exclude=website.tar.gz --exclude=.git --exclude=auth.json --exclude=magento_$MAGENTO_VERSION.sh -zcf website.tar.gz .
ls -la website.tar.gz
rsync -azc --progress -e "ssh -p $SSH_PORT" website.tar.gz ${SSH_USER}@${SSH_HOST}:${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH

# extract tar and remove tar
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH && tar -xf website.tar.gz && rm website.tar.gz"

# symlinks aka switch code
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$PUBLIC_PATH && rm -rf media"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/pub/* ${ROOT}$PUBLIC_PATH"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$PUBLIC_PATH && rm -rf bin"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/bin ${ROOT}$FILES_PATH"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/pub && rm -rf media"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH${MEDIA_PATH} ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/pub/"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH/private ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/var && rm -rf log"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH/log ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/var/"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH/logging ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/var/"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH/report ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/var/"

ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH/import ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/var/"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH/importexport ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/var/"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH/import_history ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/var/"

ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/pub && rm -rf sitemaps"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH/sitemaps ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/pub/"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svf ${ROOT}$FILES_PATH${ENV_PATH} ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/app/etc/"

# apply log rotation
echo "${ROOT}${FILES_PATH}/log/*.log " > logrotate.conf

git remote add logrotation https://bitbucket.org/weprovide/bitbucket-pipelines-magento-build.git
git fetch logrotation master
git show logrotation/master:logrotate.conf >> logrotate.conf

rsync -azc --progress -e "ssh -p $SSH_PORT" logrotate.conf ${SSH_USER}@${SSH_HOST}:${ROOT}$FILES_PATH

# get previous releasepath
export PREVRELEASEPATH=$(ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}${FILES_PATH}/ && basename \$(cd -P latestrelease 2>/dev/null && pwd) 2>/dev/null")

#symlink latest release
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH ${ROOT}$FILES_PATH/latestrelease"

# check in code for db updates
echo -e "\n\n"
dbStatus=$(ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "${SSH_PHP_VERSION} ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/bin/magento setup:db:status -v")
# compare config files for db updates, therefor get name of previous release
difference=$(ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "diff -wq ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/app/etc/config.php ${ROOT}$FILES_PATH${RELEASES_PATH}/$PREVRELEASEPATH/app/etc/config.php && echo 'no difference' || echo 'differences'")
echo $PREVRELEASEPATH
echo $difference
echo $dbStatus

# check if all modules are NOT up to date OR config files are NOT the same, DB update IS needed
if [ "$dbStatus" != "All modules are up to date." ] || [ "$difference" != "no difference" ] ; then
    echo -e "\n"
    ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/ && ${SSH_PHP_VERSION} bin/magento maintenance:enable"
    echo -e "\n"
    ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/ && ${SSH_PHP_VERSION} bin/magento setup:upgrade --keep-generated"
    echo -e "\n"
    ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/ && ${SSH_PHP_VERSION} bin/magento app:config:import"
    echo -e "\n"
    ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/ && ${SSH_PHP_VERSION} bin/magento maintenance:disable"
fi

# OPcache reset, this way was recommended by hipex
echo -e "\n\n"
echo "opcache reset";
# define sockfine
SOCKFILE=$(ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "find ${ROOT}/var/run -name 'php*.sock' -print")
# run cachetool
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "${SSH_PHP_VERSION} ${ROOT}/.bin/cachetool --fcgi=${SOCKFILE} opcache:reset"

# enable all cache
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "${SSH_PHP_VERSION} ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/bin/magento cache:clean"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "${SSH_PHP_VERSION} ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/bin/magento cache:enable"

# move previous release
if [[ -z $PREVRELEASEPATH ]]; then echo "no previous release"; else ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}${FILES_PATH}${RELEASES_PATH}/ && mv $PREVRELEASEPATH/ $PREVRELEASEPATH\_old/"; fi;

#delete lock file
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "rm ${ROOT}$FILES_PATH/build.lock"

# remove 2 releases back
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH}/ && ls -r | tail -n +3 | xargs rm -rf --"
