## Introduction

These are the buildscripts for Magento 2.2 + versions for deployment in bitbucket pipelines.


### Usage

When working with releases, make sure to move the env.php and media folder outside your release. They will be symlinked.

Make sure bitbucket has access to the server. Add the SSH key and fetch the fingerprint.

Make a dump of scopes and themes.
```
bin/magento app:config:dump scopes themes
```

Remove the `app/etc/config.php` from the `.gitignore` file.

Enable pipelines.

Add a file in your root called `bitbucket-pipelines-config.json` and add your themes/languages you want to deploy:
```json
{
  "languages":[
    "nl_NL",
    "nl_BE",
    "en_US"
  ],
  "themes":[
    "WeProvide/Custom",
    "WeProvide/Default",
    "Magento/backend"
  ]
}
```
From Magento 2.4.2-p1 you can also use `bitbucket-pipelines-themes.json` instead for a shorter deploy time. Especially when you have a lot of themes and languages.
```json
[
  {
    "language": "nl_NL",
    "themes": [
      "WeProvide/Custom"
    ]
  },
  {
    "language": "nl_BE",
    "themes": [
      "WeProvide/OtherCustom"
    ]
  },
  {
    "language": "fr_FR",
    "themes": [
      "WeProvide/Custom",
      "WeProvide/OtherCustom"
    ]
  },
  {
    "language": "en_US",
    "themes": [
      "Magento/backend",
      "WeProvide/OtherCustom"
    ]
  }
]
```


### Edit your bitbucket-pipelines.yml

Include the image for your desired PHP version like:
```
image: jahvi/docker-php-magento2:7.1
```
```
image: shoogmans/magento23-php72:latest
```
```
image: mischabraam/magento2-php73:latest
```

Add jq for reading json config file
```
- apt-get install jq
- export MAGENTO_VERSION=$(jq -r '.require["magento/product-community-edition"]' composer.json)
- if [ "$MAGENTO_VERSION" == null ] ; then export MAGENTO_VERSION=$(jq -r '.require["magento/product-enterprise-edition"]' composer.json) ; fi
- if [ "$MAGENTO_VERSION" == null ] ; then exit ; fi
```

Next, add repo for build scripts
```
- git remote add buildscripts https://bitbucket.org/weprovide/bitbucket-pipelines-magento-build.git
- git fetch buildscripts master
- git show "buildscripts/master:magento_"$MAGENTO_VERSION".sh" > "magento_"$MAGENTO_VERSION".sh"
- chmod +x "magento_"$MAGENTO_VERSION".sh"
- bash "magento_"$MAGENTO_VERSION".sh"
```


### Create pipeline variables

The build script depends on several variables like server to connect to and ssh credentials to connect with. Some variables are optional or will have a default value if you don't specify them. Below you'll find a complete list.

_You should NOT export these variables in your project's `bitbucket-pipelines.yml`. Doing so would involve a git commit to change a configuration, which is not good practice. Instead, use bitbucket's repository and deployment variables._

Use repository variables for values that are repository wide and will not change for a specific environment like production or staging. On the other hand, use deployment variables specific for each environment.

| Variable        | Required | Default value | Description |
| --------------- | -------- | ------------- | ----------- |
| SSH_PORT        |   | `22`             |  |
| SSH_USER        |   | `app`            |  |
| SSH_HOST        | * |                  |  |
| SSH_PHP_VERSION |   | `/usr/bin/php72` |  |
| ROOT            |   | `/data/web`      | Path to website root. **No ending slash.** |
| RELEASES_PATH   |   | `/releases`      |  |
| FILES_PATH      | * |                  |  |
| MEDIA_PATH      |   | `/media`         | Path to media folder, directed from within FILES_PATH. |
| ENV_PATH        |   | `/env.php`       | Path to env.php, directed from within FILES_PATH. |
| PUBLIC_PATH     | * |                  | Path to public web folder, directed from within FILES_PATH. |
| NODE_VERSION    |   | `10`             |  |
| NPM_PARAMS      |   |                  | Optional parameters to used for the `npm run build` command. |



### First installation on new server

After the first pipeline ran, the project code has been pushed to the server. The file `env.php` has been created with some values. This environment file, however, is not complete. You'll have to complete this manually. You can copy the configuration from you local project's `env.php` file or from another source. If you've used a database from another environment to start with, make sure to use the `env.php` file from that environment.



### Known issues

When you have set your minified files on the server or other missing depending configurations, use the `bin/magento config:set` command to set these vars.

```
bin/magento config:set --lock-config dev/css/minify_files 1
bin/magento config:set --lock-config dev/js/minify_files 1
```
The `--lock-config` will save the value in the config.php while `--lock-env` will save the value in the `env.php` and will not be used in deployment.
