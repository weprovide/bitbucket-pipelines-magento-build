#!/bin/sh

#set vars if not set
if [ -z $SSH_PORT ]; then SSH_PORT=22; fi
if [ -z $ENV_PATH ]; then ENV_PATH="/env.php"; fi
if [ -z $MEDIA_PATH ]; then MEDIA_PATH="/media"; fi
if [ -z $RELEASES_PATH ]; then RELEASES_PATH="/releases"; fi
if [ -z $ROOT ]; then ROOT="/data/web"; fi
if [ -z $SSH_USER ]; then SSH_USER="app"; fi
if [ -z $SSH_PHP_VERSION ]; then SSH_PHP_VERSION="/usr/bin/php72"; fi

apt-get install -y gpg
apt-get install -y build-essential
# - apt install php7.1-bcmath
docker-php-ext-install bcmath
# composer
composer install --no-dev
if [ $? -ne 0 ]
then
  RED='\033[0;31m'
  NC='\033[0m' # No Color
  echo -e "\n\n"
  echo -e "${RED}Composer install failed or threw up, aborting...${NC}"
  exit 1
fi

# npm
file="aviate.config.js"
if [ -f "$file" ]
then
    #install node
    curl -sL https://deb.nodesource.com/setup_8.x | bash -
    apt-get install -y nodejs

    #install yarn
    curl -o- -L https://yarnpkg.com/install.sh | bash -s
    export PATH=$HOME/.yarn/bin:$PATH
    yarn install
    # compile CSS and JS
	npm run build
fi

# change permissions for running commando's
chmod +x bin/magento

#disable all dev tools
bin/magento module:disable LexBeelen_Ngrok
bin/magento module:disable WeProvide_MSPAmastyFix
bin/magento module:disable MSP_DevTools
bin/magento module:disable MSP_Common

# read languages from file
languages=$(jq -r '.languages | map(.) | join(" ")' bitbucket-pipelines-config.json)
# read themes from file
themes=$(jq -r '.themes | map("--theme \(.)") | join(" ")' bitbucket-pipelines-config.json)
# deploy theme and language
export HTTPS=on
php -dmemory_limit=4G bin/magento setup:static-content:deploy -f $languages $themes

# compile code with increased memory limit
php -dmemory_limit=4G bin/magento setup:di:compile

# create new pathname
RELEASEPATH=$(date +%s)

# create a public folder for the website if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$PUBLIC_PATH"
# create a folder for the website files if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH"
# create a media folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH${MEDIA_PATH}"
# create a zetwerk folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH/zetwerk"
# create a orders folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH/orders"
# create a sitemaps folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH/sitemaps"
# create a sitemaps folder in the release if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/pub/sitemaps"
# create a releases folder if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH${RELEASES_PATH}"
# create a folder with this pathname on the server
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "mkdir -p ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH"

# create lock file to let scripts know build is busy
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "echo '' > ${ROOT}$FILES_PATH/build.lock"

# tar and move all compiled code to server
touch website.tar.gz
tar --exclude=website.tar.gz --exclude=.git --exclude=magento_$MAGENTO_VERSION.sh -zcf website.tar.gz .
ls -la website.tar.gz
rsync -azc --progress -e "ssh -p $SSH_PORT" website.tar.gz ${SSH_USER}@${SSH_HOST}:${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH

# extract tar and remove tar
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH && tar -xf website.tar.gz && rm website.tar.gz"

# symlinks aka switch code
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$PUBLIC_PATH && rm -rf media"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/pub/* ${ROOT}$PUBLIC_PATH"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$PUBLIC_PATH && rm -rf bin"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/bin ${ROOT}$FILES_PATH"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/pub && rm -rf media"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH${MEDIA_PATH} ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/pub/"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH/zetwerk ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/var/"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH/orders ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/var/"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/pub && rm -rf sitemaps"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH/sitemaps ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/pub/"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svf ${ROOT}$FILES_PATH${ENV_PATH} ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/app/etc/"

#symlink avelon
if ssh -l ${SSH_USER} ${SSH_HOST} -p $SSH_PORT "[ -d ${ROOT}$FILES_PATH/avelon ]"; then
  ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "rm -rf ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/var/tmp/avelon && ln -svfn ${ROOT}$FILES_PATH/avelon ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/var/tmp/";
fi

#symlink latest release
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH && rm -rf latestrelease"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "ln -svfn ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH ${ROOT}$FILES_PATH/latestrelease"

# check in code for db updates
dbStatus=$(ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "${SSH_PHP_VERSION} ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/bin/magento setup:db:status -v")
# compare config files for db updates, therefor get name of previous release
prevRelease=$(ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH} && ls -t | head -n 2 | tail -n 1")
difference=$(ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "diff -wq ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/app/etc/config.php ${ROOT}$FILES_PATH${RELEASES_PATH}/$prevRelease/app/etc/config.php && echo 'no difference' || echo 'differences'")
echo $prevRelease
echo $difference
echo $dbStatus

# check if all modules are NOT up to date OR config files are NOT the same, DB update IS needed
if [ "$dbStatus" != "All modules are up to date." ] || [ "$difference" != "no difference" ] ; then
 ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/ && ${SSH_PHP_VERSION} bin/magento maintenance:enable && ${SSH_PHP_VERSION} bin/magento setup:upgrade --keep-generated && ${SSH_PHP_VERSION} bin/magento app:config:import && ${SSH_PHP_VERSION} bin/magento maintenance:disable"
fi

# create file if not exists
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "echo '<?php opcache_reset();' > ${ROOT}$PUBLIC_PATH/opcache_reset.php"
# call file
SECWEBURL=$(ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "${SSH_PHP_VERSION} ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/bin/magento config:show web/secure/base_url")
WEBURL=$(ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "${SSH_PHP_VERSION} ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/bin/magento config:show web/unsecure/base_url")
echo $WEBURL
echo "opcache reset";
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "wget -O/dev/null -q ${SECWEBURL}/opcache_reset.php"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "wget -O/dev/null -q ${WEBURL}/opcache_reset.php"

#delete opcache file
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "rm ${ROOT}$PUBLIC_PATH/opcache_reset.php"

# enable all cache
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "${SSH_PHP_VERSION} ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/bin/magento cache:clean"
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "${SSH_PHP_VERSION} ${ROOT}$FILES_PATH${RELEASES_PATH}/$RELEASEPATH/bin/magento cache:enable"

# get previous releasepath
export PREVRELEASEPATH=$(ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}${FILES_PATH}${RELEASES_PATH}/ && ls -t | head -n 2 | tail -n 1")

# move previous release
if [[ -z $PREVRELEASEPATH ]]; then echo "no previous release"; else ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}${FILES_PATH}${RELEASES_PATH}/ && mv $PREVRELEASEPATH/ $PREVRELEASEPATH\_old/"; fi;

#delete lock file
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "rm ${ROOT}$FILES_PATH/build.lock"

# remove 2 releases back
ssh ${SSH_USER}@${SSH_HOST} -p $SSH_PORT "cd ${ROOT}$FILES_PATH${RELEASES_PATH}/ && ls -r | tail -n +3 | xargs rm -rf --"